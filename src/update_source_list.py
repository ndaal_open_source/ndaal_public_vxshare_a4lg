#!/usr/bin/env python3
# coding: utf-8
"""
Update VXShare Source List.

This script scrapes the URL of VXShare sources and extracts the table
containing the source downloads and converts this table into CSV format.

Author: André Breuer
Created: 2023-06-07
Modified: 2023-06-07
Tested on: macOS Ventura ARM architecture
"""
__license__ = "Apache-2.0"
__copyright__ = "Copyright (C) 2023 ndaal GmbH & Co KG"

import os

# import pandas as pd
import polars as pl
import requests
from bs4 import BeautifulSoup


def _check_response(response: requests.models.Response) -> None:
    """Check the response code of a request that has been made.

    :param response: Response of a request made.
    :type response: requests.models.Response

    :raises requests.exceptions.HTTPError: In case the status code
    indicates that the result retrieved was not ok.
    """
    if response.status_code != requests.codes.OK:  # pylint: disable=no-member
        raise requests.exceptions.HTTPError(
            f"Response is not okay. Got response code {response.status_code}")


def _get_html_soup(base_a4lg_url: str) -> BeautifulSoup:
    """Get HTML content as BeautifulSoup object.

    :param base_a4lg_url: URL to fetch HTML content from.
    :type base_a4lg_url: str

    :return: HTML content that is ready to be parsed.
    :rtype: bs4.BeautifulSoup
    """
    response = requests.get(base_a4lg_url, timeout=15)
    _check_response(response)

    return BeautifulSoup(response.text, "html.parser")


def _extract_table(bs: BeautifulSoup) -> pl.DataFrame:
    """Extract the table containing the data for downloading the sources.

    :param bs: Object for parsing HTML content.
    :type bs: BeautifulSoup

    :return: Extracted table to be used for other purposes.
    :rtype: pd.DataFrame
    """
    table = bs.find("table", {"summary": "List of download files"})

    headers = []
    data = {}
    for i, row in enumerate(table.findAll("tr")):
        if i == 0:
            for column in row.findAll("th"):
                header = column.text
                if header == "File name":
                    header = "Download Url"
                header = header.replace(" ", "")
                headers.append(header)
                data[header] = []
            continue
        for j, column in enumerate(row.findAll("td")):
            header = headers[j]
            if header == "DownloadUrl":
                for element in column.findAll("a", href=True):
                    if element.get("href") is not None:
                        data[header].append(
                            f"{base_a4lg_url}{element.get('href')}")
            else:
                data[header].append(column.text)

    return pl.DataFrame(data)


def _extract_data_columns(bs: BeautifulSoup) -> list:
    """Extract the table containing the data for downloading the sources.

    :param bs: Object for parsing HTML content.
    :type bs: BeautifulSoup

    :return: Strings indicating the column names.
    :rtype: list
    """
    column_names = bs.find("ol")
    data = []
    for column_name in column_names.findAll("li"):
        column = column_name.text
        if "hex" in column.casefold():
            split = column.split("(")[0].strip().replace("of ", "")
            data.append(split.title().replace(" ", ""))
        elif "File" in column:
            data.append(column.title().replace(" ", ""))
        else:
            data.append(column_name.text)

    # Append XXHash columns to be added later on
    data.extend(["XXHash_MD5", "XXHash_SHA-1"])

    return data


def _save_data_columns(lines: list,
                       file_path: str,
                       encoding: str = "utf-8") -> None:
    """Save list of strings as lines of a text file.

    :param lines: List of strings containing the content of the lines.
    :type lines: list
    :param file_path: Path under which to save the content.
    :type file_path: str
    :param encoding: Encoding to be used, defaults to "utf-8"
    :type encoding: str, optional
    """
    with open(file_path, "w", encoding=encoding) as f:
        for line in lines:
            f.write(f"{line}\n")


if __name__ == "__main__":
    base_a4lg_url = "https://a4lg.com/downloads/vxshare/"
    script_dir = os.path.dirname(os.path.abspath(__file__))
    table_path = os.path.join(script_dir, "sources.csv")
    columns_path = os.path.join(script_dir, "data_columns.txt")

    html = _get_html_soup(base_a4lg_url)

    table = _extract_table(html)
    table.write_csv(table_path)

    columns = _extract_data_columns(html)
    _save_data_columns(columns, columns_path)
