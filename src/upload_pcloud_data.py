#!/usr/bin/env python3
# coding: utf-8
"""
Update the hashtables for which updates are available.

Author: André Breuer
Created: 2023-06-21
Modified: 2023-06-22
Tested on: macOS Ventura ARM architecture
"""
__license__ = "Apache-2.0"
__copyright__ = "Copyright (C) 2023 ndaal GmbH & Co KG"

import logging
import os
from argparse import ArgumentParser
from argparse import Namespace
from argparse import RawTextHelpFormatter
from glob import glob

import polars as pl
from tqdm import tqdm

from misc import add_logging_arguments
from misc import log_args
from misc import setup_logger
from pCloudApi import PCloudApi


def _get_arguments() -> Namespace:
    """Parse the arguments provided via the command line.

    :return: Parsed command line arguments.
    :rtype: argparse.Namespace
    """
    parser = ArgumentParser(
        description=__doc__,
        add_help=False,
        formatter_class=RawTextHelpFormatter,
    )

    required_args = parser.add_argument_group("required_arguments")
    required_args.add_argument(
        "-u",
        "--username",
        help="Username to authenticate with pCloud",
        type=str,
        required=True,
    )
    required_args.add_argument(
        "-p",
        "--password",
        help="Password to authenticate with pCloud",
        type=str,
        required=True,
    )
    required_args.add_argument(
        "-d",
        "--directory",
        help="Path to directory in pCloud which contains all VXShare folders",
        type=str,
        required=True,
    )

    optional_args = parser.add_argument_group("optional_arguments")
    optional_args.add_argument("-h",
                               "--help",
                               action="help",
                               help="show this help message and exit")
    optional_args.add_argument(
        "-s",
        "--sources-file",
        help=(
            "Path to a CSV file containing the sources that should be updated."
            " (default: sources_to_update.csv)"),
        type=str,
        default="sources_to_update.csv",
    )
    optional_args.add_argument(
        "--dataset-dir",
        help=(
            "Path to the directory that should contain the datasets. (default:"
            " ../dataset)"),
        type=str,
        default="../dataset",
    )
    optional_args = add_logging_arguments(optional_args)

    args = parser.parse_args()
    if args.stdout_log_level is None:
        args.stdout_log_level = args.log_level

    return args


def _create_remote_directory(pcloud: PCloudApi, remote_dir: str) -> int:
    response = pcloud.get(f"createfolderifnotexists?path={remote_dir}")
    result = response.get("result", 1)
    if result != 0:
        logging.warning(
            ("Error creating the folder %s. Got the response code %s."
             " Continuing with the next folder..."),
            remote_dir,
            result,
        )

    return result


def _upload_data(
    pcloud: PCloudApi,
    sources_file: str,
    dataset_dir: str,
    remote_base_path: str,
) -> None:
    df_update = pl.read_csv(sources_file)
    for row in tqdm(df_update.iter_rows(named=True)):
        download_url = row["DownloadUrl"]
        dir_name = os.path.splitext(
            os.path.splitext(download_url.split("/")[-1])[0])[0]
        upload_dir = os.path.join(dataset_dir, dir_name)
        remote_dir = os.path.join(remote_base_path, dir_name)
        logging.debug("Considering the directory %s", upload_dir)

        if _create_remote_directory(pcloud, remote_dir) != 0:
            continue

        hash_files = glob(f"{upload_dir}/**/*.json", recursive=True)
        for hash_file_path in tqdm(hash_files):
            hash_file_name = os.path.basename(hash_file_path)
            logging.debug("Trying to upload %s", hash_file_name)
            response = pcloud.upload_files(hash_file_path, remote_dir)
            if response.get("result", 1) != 0:
                logging.warning(
                    ("Error trying to upload %s. Continuing with"
                     " the next file..."),
                    hash_file_name,
                )


if __name__ == "__main__":
    args = _get_arguments()
    setup_logger(args.log_file, args.log_level, args.stdout,
                 args.stdout_log_level)
    log_args(args)

    pcloud = PCloudApi(args.username, args.password)
    _upload_data(pcloud, args.sources_file, args.dataset_dir, args.directory)
