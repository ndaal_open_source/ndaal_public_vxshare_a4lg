#!/usr/bin/env python3
# coding: utf-8
"""
Update the hashtables for which updates are available.

Author: André Breuer
Created: 2023-06-21
Modified: 2023-06-22
Tested on: macOS Ventura ARM architecture
"""
__license__ = "Apache-2.0"
__copyright__ = "Copyright (C) 2023 ndaal GmbH & Co KG"

import hashlib
import logging
import os
import shutil
import subprocess  # nosec
import tarfile
from argparse import ArgumentParser
from argparse import Namespace
from argparse import RawTextHelpFormatter
from glob import glob

import polars as pl
import requests
from tqdm import tqdm

from misc import add_logging_arguments
from misc import log_args
from misc import setup_logger
from pCloudApi import PCloudApi


def _get_arguments() -> Namespace:
    """Parse the arguments provided via the command line.

    :return: Parsed command line arguments.
    :rtype: argparse.Namespace
    """
    parser = ArgumentParser(
        description=__doc__,
        add_help=False,
        formatter_class=RawTextHelpFormatter,
    )

    required_args = parser.add_argument_group("required_arguments")
    required_args.add_argument(
        "-u",
        "--username",
        help="Username to authenticate with pCloud",
        type=str,
        required=True,
    )
    required_args.add_argument(
        "-p",
        "--password",
        help="Password to authenticate with pCloud",
        type=str,
        required=True,
    )
    required_args.add_argument(
        "-d",
        "--directory",
        help="Path to directory in pCloud which contains all VXShare folders",
        type=str,
        required=True,
    )

    optional_args = parser.add_argument_group("optional_arguments")
    optional_args.add_argument("-h",
                               "--help",
                               action="help",
                               help="show this help message and exit")
    optional_args.add_argument(
        "-s",
        "--sources-file",
        help=(
            "Path to a CSV file containing the sources. (default: sources.csv)"
        ),
        type=str,
        default="sources.csv",
    )
    optional_args.add_argument(
        "--dataset-dir",
        help=(
            "Path to the directory that should contain the datasets. (default:"
            " ../dataset)"),
        type=str,
        default="../dataset",
    )
    optional_args = add_logging_arguments(optional_args)

    args = parser.parse_args()
    if args.stdout_log_level is None:
        args.stdout_log_level = args.log_level

    return args


def _get_existing_folders(pcloud: PCloudApi, path: str) -> list:
    folders = pcloud.get(f"listfolder?path={path}")
    return [
        item.get("name")
        for item in folders.get("metadata", {}).get("contents")
        if item is not None and item.get("isfolder")
    ]


def _extract_outdated_sources(sources_path: str,
                              existing_folders: list) -> pl.DataFrame:
    sources = pl.read_csv(sources_path)
    update = pl.DataFrame()
    for row in sources.iter_rows(named=True):
        for item in existing_folders:
            if (item in row["DownloadUrl"]
                    and "FINAL" not in row["LastUpdated"]):
                update = pl.concat([update, pl.DataFrame(row)])

    return update


def _check_response(response: requests.models.Response) -> None:
    if response.status_code != requests.codes.ok:  # pylint: disable=no-member
        raise requests.exceptions.ConnectionError(
            f"Error {response.status_code} occurred: {response.content}")


def _download_sources(data: pl.DataFrame, dataset_dir: str) -> None:
    for row in tqdm(data.iter_rows(named=True)):
        download_url = row["DownloadUrl"]
        file_name = download_url.split("/")[-1]
        logging.debug("Trying to download %s", file_name)
        save_dir = os.path.join(dataset_dir, file_name)

        response = requests.get(download_url, timeout=900)
        _check_response(response)
        if hashlib.md5(response.content).hexdigest() != row["MD5"]:  # nosec
            logging.warning(
                ("Hashes do not match. Do not consider this file. Expected"
                 " %s, found %s"),
                row["MD5"],
                hashlib.md5(response.content).hexdigest(),  # nosec
            )
            continue
        with open(save_dir, "wb") as f:
            f.write(response.content)

        logging.debug("Trying to extract the archive %s", file_name)
        with tarfile.open(save_dir, "r:xz") as tar:
            extract_files = [
                tarinfo for tarinfo in tar.getmembers()
                if ".hashes" in tarinfo.name
            ]
            tar.extractall(dataset_dir, members=extract_files,
                           filter="data")  # nosec
        shutil.move(
            os.path.join(dataset_dir, "vxshare-hashes"),
            os.path.join(
                dataset_dir,
                os.path.splitext(os.path.splitext(file_name)[0])[0],
            ),
        )
        os.remove(save_dir)


def _load_data_columns(data_columns_path: str) -> list:
    with open(data_columns_path, encoding="utf-8") as f:
        columns = f.read()

    return [item.replace("/", "_") for item in columns.split("\n") if item]


def _add_xxhashes() -> None:
    subprocess.run(["./add_xxhashes.sh"], shell=False, check=True)  # nosec


def _convert_to_hashtable(data: pl.DataFrame, columns: list,
                          dataset_dir: str) -> None:
    for row in tqdm(data.iter_rows(named=True)):
        download_url = row["DownloadUrl"]
        file_name = download_url.split("/")[-1]
        new_dir = os.path.join(
            dataset_dir,
            os.path.splitext(os.path.splitext(file_name)[0])[0])
        logging.debug("Considering files in directory %s", new_dir)

        hash_files = glob(f"{new_dir}/**/*.hashes.update", recursive=True)
        for hash_file in tqdm(hash_files):
            logging.debug("Converting file: %s to JSON", hash_file)
            hash_frame = pl.read_csv(hash_file,
                                     separator=" ",
                                     has_header=False,
                                     new_columns=columns)
            hash_frame = hash_frame.with_columns(
                source=pl.lit("https://a4lg.com/downloads/vxshare/"))
            hash_frame = hash_frame.with_columns(TrustLevel=pl.lit(7))

            hash_frame.write_json(
                hash_file.replace(".hashes.update",
                                  ".json").replace("/archives",
                                                   "").replace(".zip", ""),
                pretty=True,
                row_oriented=True,
            )


def _delete_hash_files(data: pl.DataFrame, dataset_dir: str) -> None:
    for row in data.iter_rows(named=True):
        download_url = row["DownloadUrl"]
        file_name = download_url.split("/")[-1]
        new_dir = os.path.join(
            dataset_dir,
            os.path.splitext(os.path.splitext(file_name)[0])[0])
        hash_files = glob(f"{new_dir}/**/*.hashes", recursive=True)
        hash_file_updates = glob(f"{new_dir}/**/*.hashes.update",
                                 recursive=True)
        logging.info("Removing *.hashes files in %s", new_dir)
        for hash_file in hash_files:
            logging.debug("Removing file: %s", hash_file)
            os.remove(hash_file)

        logging.info("Removing *.hashes.update files in %s", new_dir)
        for hash_file in hash_file_updates:
            logging.debug("Removing file: %s", hash_file)
            os.remove(hash_file)

        if os.path.exists(os.path.join(new_dir, "vxshare-hashes")):
            logging.info("Removing vxshare-hashes directory")
            shutil.rmtree(os.path.join(new_dir, "vxshare-hashes"))

        if os.path.exists(os.path.join(new_dir, "archives")):
            logging.info("Removing archives directory")
            shutil.rmtree(os.path.join(new_dir, "archives"))


def _update_sources(data: pl.DataFrame, dataset_dir: str,
                    data_columns_path: str) -> None:
    logging.info("Downloading sources...")
    _download_sources(data, dataset_dir)
    logging.info("Adding XXHashes...")
    _add_xxhashes()
    logging.info("Creating JSON files...")
    columns = _load_data_columns(data_columns_path)
    _convert_to_hashtable(data, columns, dataset_dir)
    logging.info("Deleting old .hashes files.")
    _delete_hash_files(data, dataset_dir)


if __name__ == "__main__":
    args = _get_arguments()
    setup_logger(args.log_file, args.log_level, args.stdout,
                 args.stdout_log_level)
    log_args(args)

    pcloud = PCloudApi(args.username, args.password)
    data_columns_path = "data_columns.txt"

    dirs = _get_existing_folders(pcloud, args.directory)
    sources_frame = _extract_outdated_sources(args.sources_file, dirs)
    sources_frame.write_csv("sources_to_update.csv")
    _update_sources(sources_frame, args.dataset_dir, data_columns_path)
