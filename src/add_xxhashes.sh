#!/usr/bin/env bash

set -o errexit
set -o errtrace
set -o nounset

script_path="$(dirname "$(readlink -f "${0}")")"
directories=$(find ../dataset -maxdepth 1 -type d -name "vxshare-hashes*")

for directory in $directories; do
  echo "Considering directory: $directory"
  cd "$directory"
  files=$(find . -type f -name "*.hashes")
  for file in $files; do
    echo "  Working on $file"
    paste -d ' ' "$file" <(cut -d ' ' -f 5 "$file" | xargs hash_string | awk '{ print $1 }') <(cut -d ' ' -f 8 "$file" | xargs hash_string | awk '{ print $1 }') >"$(basename "$file").update"
  done
  cd "$script_path"
done
