#!/usr/bin/env python3
# coding: utf-8
"""
Provide miscellaneous functions.

Author: André Breuer
Created: 2023-06-22
Modified: 2023-06-22
Tested on: macOS Ventura ARM architecture
"""
__license__ = "Apache-2.0"
__copyright__ = "Copyright (C) 2023 ndaal GmbH & Co KG"

import logging
import sys
from argparse import BooleanOptionalAction
from argparse import Namespace
from argparse import _ArgumentGroup
from typing import Optional


def add_logging_arguments(arg_group: _ArgumentGroup) -> _ArgumentGroup:
    """Add arguments which provide functionality for logging to argument group.

    :param arg_group: Argument group to add arguments to.
    :type arg_group: argparse._ArgumentGroup
    :return: Argument group with added arguments.
    :rtype: argparse._ArgumentGroup
    """
    arg_group.add_argument(
        "-l",
        "--log-level",
        type=str,
        help=(
            "Specify the logging level.\nPossible options are:\n  - 'debug'\n "
            " - 'info'\n  - 'warning'\n  - 'error'\n  - 'critical'\n(default:"
            " 'info')"),
        choices=["debug", "info", "warning", "error", "critical"],
        default="info",
        metavar="LOG_LEVEL",
    )
    arg_group.add_argument(
        "-lf",
        "--log-file",
        type=str,
        help=("Path to a logging file to be used. If no logging file is"
              " specified, then the logs are not written to any file."),
        default=None,
    )
    arg_group.add_argument(
        "--stdout",
        type=bool,
        help=("If specified the logs are written to STDOUT in addition to what"
              " is specified to a logging file."),
        action=BooleanOptionalAction,
        default=True,
    )
    arg_group.add_argument(
        "--stdout-log-level",
        type=str,
        help=("Specify the logging level for writing to STDOUT.\nPossible"
              " options are:\n  - 'debug'\n  - 'info'\n  - 'warning'\n  -"
              " 'error'\n  - 'critical'\nIf no log level is specified the"
              " logging level will be the same as specified in the --log-level"
              " argument"),
        choices=["debug", "info", "warning", "error", "critical"],
        default=None,
        metavar="LOG_LEVEL",
    )

    return arg_group


def _convert_log_level(log_level: str) -> int:
    """Validate a log level string and convert it into a numeric value.

    :param log_level: Describes the log level that should be used.
    :type log_level: str

    :raises ValueError: If the log level is not a valid level within
        the logging library.

    :return: Numeric log level expected by a logger
    :rtype: int
    """
    numeric_level = getattr(logging, log_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError(f"The log level '{log_level}' is not valid...")

    return numeric_level


def _prepare_logging_handler(log_file: Optional[str] = None,
                             log_level: str = "info") -> logging.Handler:
    """Prepare a logging handler.

    The handler will be selected dynamically as a stream handler or a
    file handler depending on the arguments given. Further the format
    for logging will be set as well as the logging level for that
    particular handler.

    :param log_file: Name of the file to be logged. If this is not
        given then a stream handler instead of a file handler will be
        used, defaults to None
    :type log_file: str, optional
    :param log_level: Log level that should be used for logging to this
        handler, defaults to "info"
    :type log_level: str, optional

    :return: Prepared handler that has logging level, as well as format
        set.
    :rtype: logging.Handler
    """
    if log_file is None:
        handler = logging.StreamHandler(stream=sys.stdout)
    else:
        handler = logging.FileHandler(log_file)

    formatter = logging.Formatter("%(asctime)s; %(levelname)s; %(message)s",
                                  "%Y-%m-%d %H:%M:%S")
    handler.setFormatter(formatter)
    handler.setLevel(_convert_log_level(log_level))

    return handler


def setup_logger(
    log_file: Optional[str] = None,
    log_level: str = "warning",
    stdout_logging: bool = True,
    stdout_log_level: str = "info",
) -> None:
    """Set up the logging according to the specified arguments.

    :param log_file: File to log to. If no file is specified no file
        logger will be created, defaults to None
    :type log_file: str, optional
    :param log_level: Log level for the file logger, defaults to "warning"
    :type log_level: str, optional
    :param stdout_logging: Specifies whether logging to the STDOUT
        stream should be enabled, defaults to True
    :type stdout_logging: bool, optional
    :param stdout_log_level: Log level for the stream logging. Only
        takes effect if logging to STDOUT is activated, defaults to
        "info"
    :type stdout_log_level: str, optional
    """
    logging.basicConfig(level=_convert_log_level("debug"))
    logging.getLogger().handlers.clear()
    handlers = []
    if log_file is not None:
        handlers.append(_prepare_logging_handler(log_file, log_level))
    if stdout_logging:
        handlers.append(
            _prepare_logging_handler(log_file=None,
                                     log_level=stdout_log_level))

    for handler in handlers:
        logging.getLogger().addHandler(handler)

    logging.debug("Starting a new run...")


def log_args(args: Namespace) -> None:
    """Log the arguments provided via CLI for debugging.

    :param args: Arguments provided via CLI.
    :type args: argparse.Namespace
    """
    logging.debug("Settings used for the current run")
    for key, value in vars(args).items():
        if key in ["username", "password"]:
            continue
        logging.debug("Argument: %s, Value: %s", key, value)
