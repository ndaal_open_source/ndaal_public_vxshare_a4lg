#!/usr/bin/env python3
# coding: utf-8
"""
Provide interface for pCloud API.

Author: André Breuer
Created: 2023-06-19
Modified: 2023-06-22
Tested on: macOS Ventura ARM architecture
"""
__license__ = "Apache-2.0"
__copyright__ = "Copyright (C) 2023 ndaal GmbH & Co KG"

import hashlib
import os
from time import time
from typing import Union

import requests
from requests_toolbelt import MultipartEncoder


class PCloudApi:
    """Interface for pCloud API."""

    def __init__(
        self,
        username: str,
        password: str,
        timeout: int = 10,
        auth_refresh: int = 600,
    ) -> None:
        """Initialize the PCloudApi class.

        :param username: Username to be used for authentication against
            the API.
        :type username: str
        :param password: Password to be used for the authentication
            against the API.
        :type password: str
        :param timeout: Timeout that should be used by default for
            making requests, defaults to 10
        :type timeout: int, optional
        :param auth_refresh: Interval in seconds to be used for
            refreshing the authenticaiton token, defaults to 600
        :type auth_refresh: int, optional
        """
        self._api_url = "https://eapi.pcloud.com"
        self._AUTH_REFRESH = auth_refresh

        self._username = username
        self._pass = password
        self._auth = None
        self._auth_time = 0
        self._timeout = timeout

        self._set_auth_code()

    def _get_digests(self) -> tuple:
        response = requests.get(f"{self._api_url}/getdigest",
                                timeout=self._timeout)
        self._check_response(response)
        digest = response.json().get("digest")

        if digest is None:
            raise ValueError(
                "No digest present. It seems that something went wrong...")

        username_sha = hashlib.sha1(  # nosec
            self._username.encode()).hexdigest()
        passworddigest = hashlib.sha1(  # nosec
            f"{self._pass}{username_sha}{digest}".encode()).hexdigest()

        return digest, passworddigest

    def _set_auth_code(self) -> None:
        # pylint: disable=line-too-long
        digest, passworddigest = self._get_digests()
        self._auth_time = time()
        response = requests.get(
            f"{self._api_url}/userinfo?getauth=1&logout=1&username={self._username}&digest={digest}&passworddigest={passworddigest}",
            timeout=self._timeout,
        )
        self._check_response(response)

        self._auth = response.json().get("auth")

    def _check_auth_code(self) -> None:
        if time() - self._auth_time >= self._AUTH_REFRESH:
            self._set_auth_code()

    @staticmethod
    def _check_requests_method(method: str) -> None:
        available_methods = [
            "GET",
            "OPTIONS",
            "HEAD",
            "POST",
            "PUT",
            "PATCH",
            "DELETE",
        ]
        if method not in available_methods:
            raise ValueError(
                f"Provided method '{method}' is not valid. Please choose from"
                f" {available_methods}")

    def _get_full_api_url(self, endpoint: str) -> str:
        url = f"{self._api_url}/{endpoint}"
        return (f"{url}&auth={self._auth}"
                if "?" in endpoint else f"{url}?auth={self._auth}")

    def request(self, endpoint: str, method: str = "GET", **kwargs) -> dict:
        """Send the specified type of request.

        :param endpoint: API endpoint to query including parameters
        :type endpoint: str
        :param method: Method to be used for the query, defaults to "GET"
        :type method: str, optional

        :return: JSON content of the response
        :rtype: dict
        """
        self._check_requests_method(method)
        if kwargs.get("timeout") is None:
            kwargs["timeout"] = self._timeout

        self._check_auth_code()
        req_fun = getattr(requests, method.lower())
        url = self._get_full_api_url(endpoint)

        response = req_fun(url, **kwargs)
        self._check_response(response)

        return response.json()

    def get(self, endpoint: str, **kwargs) -> dict:
        """Send a GET request.

        :param endpoint: API endpoint to query including parameters
        :type endpoint: str

        :return: JSON content of the response
        :rtype: dict
        """
        return self.request(endpoint, "GET", **kwargs)

    def post(self, endpoint: str, **kwargs) -> dict:
        """Send a POST request.

        :param endpoint: API endpoint to query including parameters
        :type endpoint: str

        :return: JSON content of the response
        :rtype: dict
        """
        return self.request(endpoint, "POST", **kwargs)

    def put(self, endpoint: str, **kwargs) -> dict:
        """Send a PUT request.

        :param endpoint: API endpoint to query including parameters
        :type endpoint: str

        :return: JSON content of the response
        :rtype: dict
        """
        return self.request(endpoint, "PUT", **kwargs)

    def patch(self, endpoint: str, **kwargs) -> dict:
        """Send a PATCH request.

        :param endpoint: API endpoint to query including parameters
        :type endpoint: str

        :return: JSON content of the response
        :rtype: dict
        """
        return self.request(endpoint, "PATCH", **kwargs)

    def delete(self, endpoint: str, **kwargs) -> dict:
        """Send a DELETE request.

        :param endpoint: API endpoint to query including parameters
        :type endpoint: str

        :return: JSON content of the response
        :rtype: dict
        """
        return self.request(endpoint, "DELETE", **kwargs)

    @staticmethod
    def _pre_process_upload_files(files: Union[str, list]) -> list:
        """Preprocess the file(s) s.t. they are in expected upload format.

        :param files: Either path to a single file or a list to multiple
            file paths.
        :type files: Union[str, list]

        :raises TypeError: If passed files are not of type str or list.

        :return: List of tuples where first element contains "file" for
            the parameter to be passed in the URL and the second being
            a tuple where the first element is the name of the file and
            the second one the binary content of the file.
        :rtype: list
        """
        if isinstance(files, str):
            files = [files]
        elif not isinstance(files, list):
            raise TypeError(
                "Passed files are of invalid type. Expected either str or"
                f" list, foudn {type(files)}")

        return [("file", (os.path.split(f)[1], open(f, "rb"))) for f in files]

    def upload_files(self, files: Union[str, list], remote_path: str,
                     **kwargs) -> dict:
        """Upload a single file or a list of files.

        :param files: Either path to a single file or a list to multiple
            file paths.
        :type files: Union[str, list]
        param remote_path:

        :return: JSON content of the response
        :rtype: dict
        """
        upload_files = self._pre_process_upload_files(files)
        kwargs["path"] = remote_path
        fields = list(kwargs.items())
        fields.extend(upload_files)
        encoder = MultipartEncoder(fields=fields)

        return self.post(
            "uploadfile",
            headers={"Content-Type": encoder.content_type},
            data=encoder,
        )

    @staticmethod
    def _check_response(response: requests.models.Response) -> None:
        if (response.status_code != requests.codes.ok  # pylint: disable=no-member
            ):
            try:
                data = response.json()
                raise requests.exceptions.ConnectionError(
                    f"Failed with error code: {data.get('result')} and message"
                    f" '{data.get('error')}'")
            except requests.exceptions.JSONDecodeError as e:
                raise requests.exceptions.JSONDecodeError(e)

    @property
    def api_url(self) -> str:
        """Porperty defining the API URL of pCloud.

        :return: API URL of pCloud.
        :rtype: str
        """
        return self._api_url

    @api_url.deleter
    def api_url(self) -> None:
        del self._api_url
